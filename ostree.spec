#needsrootforbuild
Name:           ostree
Version:        2025.1
Release:        3
Summary:        A tool like git for operating system binaries
License:        LGPL-2.0-or-later
URL:            https://ostreedev.github.io/ostree/
Source0:        https://github.com/ostreedev/%{name}/releases/download/v%{version}/libostree-%{version}.tar.xz

BuildRequires: gcc make
BuildRequires: pkgconfig(bash-completion)
BuildRequires: pkgconfig(e2p)
BuildRequires: pkgconfig(fuse3) >= 3.1.1
BuildRequires: pkgconfig(gio-unix-2.0) >= 2.66.0
BuildRequires: pkgconfig(gpg-error)
BuildRequires: pkgconfig(gpgme) >= 1.8.0
BuildRequires: pkgconfig(libarchive) >= 2.8.0
BuildRequires: pkgconfig(libcrypto) >= 1.0.1
BuildRequires: pkgconfig(libcurl) >= 7.29.0
BuildRequires: pkgconfig(liblzma) >= 5.0.5
BuildRequires: pkgconfig(libselinux) >= 2.1.13
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(mount) >= 2.23.0
BuildRequires: pkgconfig(systemd)
BuildRequires: pkgconfig(zlib)
BuildRequires: bison
BuildRequires: gobject-introspection-devel
BuildRequires: gtk-doc
BuildRequires: /usr/bin/xsltproc
BuildRequires: /usr/sbin/grub2-mkconfig
%{?systemd_requires}

Requires:       dracut systemd-units gnupg2
%ifarch x86_64
Requires: grub2
%else
%ifnarch ppc64le
Requires: grub2-efi
%endif
%endif

Provides:       ostree-libs ostree-grub2
Provides:       ostree-libs%{?_isa} = %{version}-%{release}

%description
This project is now known as "libostree", though it is still appropriate to use the previous name: "OSTree"
(or "ostree"). The focus is on projects which use libostree's shared library, rather than users directly
invoking the command line tools (except for build systems). However, in most of the rest of the documentation,
we will use the term "OSTree", since it's slightly shorter, and changing all documentation at once is impractical.

%package devel
Summary: Development headers for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package includes the header files for the %{name} library.

%package_help

%prep
%autosetup -n lib%{name}-%{version} -p1

%build
%configure --with-selinux --with-curl --with-openssl \
           --enable-gtk-doc \
           --without-soup \
	   --without-avahi \
           --with-dracut=yesbutnoconf 
%make_build

%install
%make_install
%delete_la

%post
%systemd_post ostree-remount.service
 
%preun
%systemd_preun ostree-remount.service

%files
%doc README.md
%license COPYING
%{_bindir}/%{name}
%{_bindir}/rofiles-fuse
%{_datadir}/%{name}
%{_datadir}/bash-completion/*/*
%{_prefix}/lib/dracut/modules.d/98ostree
%{_systemdgeneratordir}/*
%{_unitdir}/*
%{_tmpfilesdir}/*
%{_prefix}/lib/%{name}
%{_libexecdir}/lib%{name}
%{_sysconfdir}/%{name}
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*
%{_sysconfdir}/grub.d/*

%files devel
%{_libdir}/*.so
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0/*.gir

%files help
%{_mandir}/man?/*
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Sat Feb 08 2025 Funda Wang <fundawang@yeah.net> - 2025.1-3
- disabel avahi feature from configure makes it really disabled

* Sat Feb 08 2025 zhangyao <zhangyao108@huawei.com> - 2025.1-2
- disable avahi feature

* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 2025.1-1
- update to 2025.1

* Sat Nov 09 2024 Funda Wang <fundawang@yeah.net> - 2024.9-1
- update to 2024.9

* Sun Sep 22 2024 Funda Wang <fundawang@yeah.net> - 2024.8-1
- update to 2024.8

* Thu Aug 15 2024 dillon chen <dillon.chen@gmail.com> - 2024.7-1
- update to 2024.7

* Fri Jul 5 2024 warlcok <hunan@kylinos.cn> - 2024.6-1
- update to 2024.6

* Fri Mar 15 2024 xuhe <xuhe@kylinos.cn> - 2024.5-1
- update to 2024.5

* Mon Nov 27 2023 jiahua.yu <jiahua.yu@shingroup.cn> - 2023.5-4
- init support for ppc64le

* Tue Sep 19 2023 jiangchuangang <jiangchuangang@huawei.com> - 2023.5-3
- fix test-rofiles-fuse.sh failed

* Fri Aug 11 2023 liyunfei <liyunfei33@huawei.com> - 2023.5-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add clang compile support

* Wed Jul 12 2023 jiangchuangang <jiangchuangang@huawei.com> - 2023.5-1
- update to 2023.5

* Sun Jan 29 2023 jiangchuangang <jiangchuangang@huawei.com> - 2022.7-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2022.7
  add skip-if-etc-mtab-is-not-a-symlink-of-proc-self-mounts.patch for test failed

* Mon Oct 17 2022 yangmingtai <yangmingtai@huawei.com> - 2021.6-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add Buildrequires elfutils-extra

* Tue Feb 8 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 2021.6-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update ostree to 2021.6

* Tue Jul 20 2021 wangchen <wangchen137@huawei.com> - 2020.8-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Delete unnecessary gdb from BuildRequires

* Fri Feb 5 2021 panxiaohe <panxiaohe@huawei.com> - 2020.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix failed tests about test-pull-summary-sigs.sh 

* Mon Jan 25 2021 zoulin <zoulin13@huawei.com> - 2020.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update ostree to 2020.8

* Wed Aug 12 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 2020.4-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:DO not run testcase test-libarchive-import,because selinux is off.

* Sat Jul 25 2020 linwei <linwei54@huawei.com> - 2020.4-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update ostree to 2020.4

* Sat Mar 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 2019.4-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add man files into help package

* Tue Feb 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2019.4-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable check

* Sat Oct 19 2019 shenyangyang <shenyangyang4@huawei.com> - 2019.4-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add the version and release of ostree-libs%{?_isa}

* Fri Oct 18 2019 shenyangyang <shenyangyang4@huawei.com> - 2019.4-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add ostree-libs%{?_isa} that required by flatpak

* Mon Oct 14 2019 shenyangyang <shenyangyang4@huawei.com> - 2019.4-2
- Type: enhancement
- ID: NA
- SUG: NA
- DESC:delete unneeded build requires

* Mon Oct 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 2019.4-1
- Package Init
